# Project Description
This repository contains a web application that offers a web UI through which it is possible to configure
an Icinga Server nodes and services.

## Requirements
In order to manage the project correctly it is required to have:
- Java 7 installed (click [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) to download the software);
- Maven 3 installed (click [here](https://maven.apache.org/download.cgi) to download the software) and configured to use Java 8;
- An Icinga Server up and running;
- A JBoss AS (the project is tested with version 7.1.1);

## Artifacts
This module's packaging produces:
 * A deployable EAR artifact _./ear/target/ear-<version>-ear_;

## How to use
See root folder *REAMDE.md* for an insight.

## Setup
Before compilation it is necessary to configure properly some project properties. In fact it is necessary
to define:

1. Myfaces JNDI (see env-entry within _./designer/src/main/webapp/WEB-INF/web.xml_);
2. TODO: other JNDI if any

Once the project is configured and built it is possible to deploy the Enterprise Archive (EAR) within
the Application Server (see _./webapp/ear/target/ear-<version>-ear_).

Further it is necessary to configure the JVM used by JBoss and add the Icinga Server certificate if it is a self-signed certificate.
An exhaustive procedure to configure the JVM is available in the paragraph Setup of the file _../icinga/README.md_.

## Tips
During development it is possible to use some tips that can simplify developer work.
* Your remote server maybe does not allow remote connections to certain ports. In order to verify if
a port is remotely accessible use:
   ```
    $ netstat <server-ip> <port>
   ```
   
   and if the port is not accessible it is possible to create a tunnel through ssh to allow you to access
   remotely. 
   
   For example for tunneling JBoss admin console port (default 9990) and HTTP port (default 8080) if 
   not public use ssh tool with the following command

   ```
    $ ssh -L <local-port>:localhost:<remote-port> <user>@<jboss-server-hostname>
    #############################################################
    # e.g. ssh -L 9990:localhost:9990 centos@unimore.icinga.edu
    #############################################################
   ```
   **N.B.** It is necessary to tunnel port 9999 in order to allow remote deploy.
   
* In order to enable remote access to a specific port to your server it is necessary to add a rule 
in the firewall and restart it;   
 
* Remote debug your application:
   * Enable remote debug following the guide [here](https://developer.jboss.org/wiki/JBossAS7UsingJPDAToDebugTheASSourceCode);
   * Configure your IDE for **Remote Debug**;

