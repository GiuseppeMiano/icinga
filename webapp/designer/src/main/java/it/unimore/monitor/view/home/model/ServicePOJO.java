package it.unimore.monitor.view.home.model;

import com.google.gson.annotations.Expose;
import it.unimore.monitor.client.configuration.ServiceConfiguration;

/*
=================================================================================================
Created on 19/11/2017
*
NodePOJO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class ServicePOJO {
  @Expose(serialize = false)
  private String name;
  @Expose(deserialize = false)
  private ServiceConfiguration attrs;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ServiceConfiguration getAttrs() {
    return attrs;
  }

  public void setAttrs(ServiceConfiguration attrs) {
    this.attrs = attrs;
  }
}
