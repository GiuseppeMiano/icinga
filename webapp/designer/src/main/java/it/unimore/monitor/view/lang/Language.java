package it.unimore.monitor.view.lang;

import java.io.Serializable;
import java.util.Locale;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 * =================================================================================================
 * Created on 27/12/2017 Copyright (C) 2017 Doxee S.p.A. C.F. - P.IVA: IT02714390362. All Rights
 * Reserved
 *
 * Language.java
 *
 * Comments are welcome.
 *
 * Author: gmiano <gmiano@doxee.com>
 *
 * =================================================================================================
 */

public class Language implements Serializable {
  private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

  public Locale getLocale() {
    return locale;
  }

  public String getLanguage() {
    return locale.getLanguage();
  }

  //when dropdown value gets changed - so the language and locale
  public void onLanguageChange(ValueChangeEvent vce) {
    if (vce != null) {
      String l = vce.getNewValue().toString();
      setLanguage(l);
      locale = new Locale(l);
      FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    } else {
      FacesContext.getCurrentInstance().getViewRoot()
          .setLocale(Locale.ENGLISH);
    }
  }

  public void setLanguage(String language) {
    locale = new Locale(language);
    FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
  }
}
