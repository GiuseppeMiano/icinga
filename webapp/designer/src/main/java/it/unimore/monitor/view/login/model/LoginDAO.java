package it.unimore.monitor.view.login.model;

import it.unimore.monitor.client.http.Icinga2ListRequest;
import it.unimore.monitor.client.http.Icinga2SupportedAction;
import it.unimore.monitor.utils.BeanUtils;
import it.unimore.monitor.utils.IcingaUtils;
import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 05/11/2017
*
NodesDAO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class LoginDAO {
  private final static Logger log = Logger.getLogger(LoginDAO.class);
  private LoginPOJO login;

  public String login() throws Exception {
    log.info("User " + login.getUsername() + " is trying to login.");
    boolean result = sign();
    if (result) {
      // get Http Session and store username
      HttpSession session = BeanUtils.getSession();
      session.setAttribute("username", login.getUsername());
      log.info("Login succeeded.");
      return "home";
    } else {
      log.info("Login failed.");
      BeanUtils.printMessage("Invalid Login! Please try again!", FacesMessage.SEVERITY_WARN);
      // invalidate session, and redirect to other pages
      //message = "Invalid Login. Please Try Again!";
      return "login";
    }
  }

  public String logout() throws IOException {
    log.info("User " + login.getUsername() + " is logging out.");
    HttpSession session = BeanUtils.getSession();
    session.invalidate();
    log.info("User " + login.getUsername() + " logged out.");
    setLogin(null);
    return "login";
  }

  private boolean sign() throws Exception {
    try {
      Icinga2ListRequest listRequest = (Icinga2ListRequest) IcingaUtils.getIcingaRequestFactory(login)
          .getRequest(Icinga2SupportedAction.LIST);
      int response = listRequest.authenticate().getResponseCode();
      return response == 200;
    } catch (Exception e) {
      log.error("Error during server authentication request sending.", e);
      throw new Exception("Error creating Icinga 2 server URL.");
    }
  }

  public void setLogin(LoginPOJO login) {
    this.login = login;
  }

  public LoginPOJO getLogin() {
    return this.login;
  }
}
