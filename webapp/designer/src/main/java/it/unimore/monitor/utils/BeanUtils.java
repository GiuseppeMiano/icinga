package it.unimore.monitor.utils;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
=================================================================================================
Created on 15/12/2017
*
BeanUtils.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class BeanUtils {
  public static HttpSession getSession() {
    return (HttpSession)
        FacesContext.getCurrentInstance().getExternalContext().getSession(false);
  }

  public static String getUserId() {
    HttpSession session = getSession();
    if ( session != null )
      return (String) session.getAttribute("userid");
    else
      return null;
  }

  public static HttpServletRequest getRequest() {
    return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
  }

  public static String getUserName() {
    HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    return  session.getAttribute("username").toString();
  }

  public static void printMessage(String msg) {
    printMessage(msg, FacesMessage.SEVERITY_INFO);
  }

  public static void printMessage(String msg, Severity severity) {
    String header;
    if (severity == FacesMessage.SEVERITY_ERROR) {
      header = "Error!";
    } else if (severity == FacesMessage.SEVERITY_FATAL) {
      header = "Fatal!";
    } else if (severity == FacesMessage.SEVERITY_INFO) {
      header = "Info!";
    } else {
      header = "Warn!";
    }
    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, header, msg));
  }
}
