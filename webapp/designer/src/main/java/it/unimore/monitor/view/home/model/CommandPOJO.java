package it.unimore.monitor.view.home.model;

import com.google.gson.annotations.Expose;
import it.unimore.monitor.client.configuration.CommandConfiguration;

/*
=================================================================================================
Created on 19/11/2017
*
CommandPOJO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class CommandPOJO {
  @Expose(serialize = false)
  private String name;
  @Expose(deserialize = false)
  private CommandConfiguration attrs;
  @Expose(serialize = false)
  private String type;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CommandConfiguration getAttrs() {
    return attrs;
  }

  public void setAttrs(CommandConfiguration attrs) {
    this.attrs = attrs;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
