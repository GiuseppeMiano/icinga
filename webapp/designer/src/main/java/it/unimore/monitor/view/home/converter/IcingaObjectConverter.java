package it.unimore.monitor.view.home.converter;

import it.unimore.monitor.view.home.model.HomeDAO;
import it.unimore.monitor.view.home.model.NodePOJO;
import it.unimore.monitor.view.home.model.CommandPOJO;
import it.unimore.monitor.view.home.model.ServicePOJO;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 03/12/2017
*
IcingaObjectConverter.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class IcingaObjectConverter implements Converter {
  private final static Logger log = Logger.getLogger(IcingaObjectConverter.class);

  @Override
  public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s)
      throws ConverterException {
    if (s == null || s.isEmpty()) {
      return null;
    }
    else {
      String id = (String) UIComponent.getCurrentComponent(facesContext).getAttributes().get("converterType");
      try {
        HomeDAO manager = (HomeDAO) facesContext.getViewRoot().getViewMap().get("manager");
        switch (id) {
          case "node": return manager.getNodes().get(Integer.parseInt(s));
          case "command": return manager.getCommands().get(Integer.parseInt(s));
          case "service": return manager.getNode().getServices().get(Integer.parseInt(s));
          default: throw new RuntimeException("Configure properly the id of the select menu.");
        }
      } catch(NumberFormatException e) {
        log.error("Get object of type " + id + " conversion Error", e);
        throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
            "Get object of type " + id + " conversion Error", "Not a valid " + id + "."));
      }
    }
  }

  @Override
  public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
      throws ConverterException {
    if(o == null ) {
      return "";
    }
    else if ( !(o instanceof CommandPOJO || o instanceof NodePOJO || o instanceof ServicePOJO)) {
      log.error("Invalid conversion object type.");
      throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
          "Get Icinga Object name conversion Error", "Not a valid type. Try to reload."));
    }
    else {
      HomeDAO manager = (HomeDAO) facesContext.getViewRoot().getViewMap().get("manager");
      String id = (String) UIComponent.getCurrentComponent(facesContext).getAttributes().get("converterType");
      switch (id) {
        case "node": return String.valueOf(manager.getNodes().indexOf(o));
        case "command": return String.valueOf(manager.getCommands().indexOf(o));
        case "service": return String.valueOf(manager.getNode().getServices().indexOf(o));
        default: log.error("Get String of type " + id + " conversion Error");
                  throw new RuntimeException("Configure properly the id of the select menu.");
      }
    }
  }
}
