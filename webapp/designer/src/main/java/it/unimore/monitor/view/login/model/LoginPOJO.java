package it.unimore.monitor.view.login.model;

/*
=================================================================================================
Created on 05/11/2017
*
HomePOJO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/


public class LoginPOJO {
  private String username;
  private String password;

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
