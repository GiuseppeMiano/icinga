package it.unimore.monitor.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import it.unimore.monitor.client.serialization.strategies.DeserializationExcludeAnnotationStrategy;
import it.unimore.monitor.client.serialization.strategies.SerializationExcludeAnnotationStrategy;
import it.unimore.monitor.view.home.model.CommandPOJO;
import java.lang.reflect.Type;

/*
=================================================================================================
Created on 15/12/2017
*
SerializationUtils.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class SerializationUtils {
  private final static Gson gson = new GsonBuilder()
      .addSerializationExclusionStrategy(new SerializationExcludeAnnotationStrategy())
      .addDeserializationExclusionStrategy(new DeserializationExcludeAnnotationStrategy()).create();
  private final static JsonParser parser = new JsonParser();

  public static Gson getGson() {
    return gson;
  }


  public static ServiceConfiguration serializeNodeService (CommandPOJO command) {
    JsonObject attrsJson = new JsonObject();
    attrsJson.addProperty(camelCaseToUnderscore(command.getType()), command.getName());
    ServiceConfiguration config = deserialize(serialize(attrsJson), new TypeToken<ServiceConfiguration>(){}.getType());
    return config;
  }

  public static JsonElement parse(String json) {
    return parser.parse(json);
  }

  public static String camelCaseToUnderscore (String str) {
    String regex = "([a-z])([A-Z]+)";
    String replacement = "$1_$2";
    return str.replaceAll(regex, replacement).toLowerCase();
  }

  public static <T> String serialize(T object) {
    return gson.toJson(object);
  }

  public static <T> T deserialize(String json, Type t) {
    return gson.fromJson(json, t);
  }
}
