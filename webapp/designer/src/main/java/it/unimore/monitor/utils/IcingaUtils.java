package it.unimore.monitor.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import it.unimore.monitor.client.http.Icinga2RequestFactory;
import it.unimore.monitor.client.http.Icinga2Response;
import it.unimore.monitor.view.login.model.LoginPOJO;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 15/12/2017
*
IcingaUtils.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class IcingaUtils {
  private final static Logger log = Logger.getLogger(IcingaUtils.class);
  private final static String PORT_JNDI = "java:global/icinga/port";
  private final static String HOSTNAME_JNDI = "java:global/icinga/hostname";

  public static Icinga2RequestFactory getIcingaRequestFactory(LoginPOJO login) throws RuntimeException {
    log.info("Creating Icinga 2 requests factory...");
    Icinga2ClientBuilder builder = Icinga2ClientBuilder
        .create()
        .withCredentials(login.getUsername(), login.getPassword());
    try {
      InitialContext ctx = new InitialContext();
      String hostname = (String) ctx.lookup(HOSTNAME_JNDI);
      int port = (Integer) ctx.lookup(PORT_JNDI);
      // If arrives here create a custom client with all custom parameters
      log.info("JNDI will be used for setup: hostname=" + hostname + ", password=" + login.getPassword());
      builder.withPort(port)
          .withHostname(hostname);
    } catch (NamingException e) {
      log.warn("No JNDI configured, access with default port and hostname will be used...");
    }
    try {
      return new Icinga2RequestFactory(builder.build());
    } catch (MalformedURLException e) {
      log.error("An irreversible error occurred. Execution stops.");
      throw new RuntimeException("Error creating Icinga 2 server URL.");
    }
  }

  public static JsonArray getResults(Icinga2Response icinga2Response) {
    if (icinga2Response.getResponseCode() == 200) {
      try {
        JsonElement response = SerializationUtils.parse(icinga2Response.getResponsePayload());
        return response.getAsJsonObject().getAsJsonArray("results");
      } catch (IOException e) {
        log.error("An error occurred getting icinga 2 response payload.");
        throw new RuntimeException(e);
      }
    } else {
      log.warn("The request response from Icinga is not a 200 HTTP response.");
      throw new RuntimeException("Response status: " + icinga2Response.getResponseCode());
    }
  }

  public static void getResponse(Icinga2Response response) throws IOException {
    log.warn("An error occurred with the request. The response code is: " + response.getResponseCode());
    log.warn("The response is: " + response.getResponsePayload());
    throw new RuntimeException(response.getResponseReason());
  }
}
