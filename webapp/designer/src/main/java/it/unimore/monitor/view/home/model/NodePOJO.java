package it.unimore.monitor.view.home.model;

import com.google.gson.annotations.Expose;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import java.util.List;

/*
=================================================================================================
Created on 19/11/2017
*
NodePOJO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class NodePOJO {
  @Expose(serialize = false)
  private String name;
  @Expose(deserialize = false)
  private NodeConfiguration attrs;
  @Expose(serialize = false)
  private List<ServicePOJO> services;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public NodeConfiguration getAttrs() {
    return attrs;
  }

  public void setAttrs(NodeConfiguration attrs) {
    this.attrs = attrs;
  }

  public List<ServicePOJO> getServices() {
    return services;
  }

  public void setServices(List<ServicePOJO> services) {
    this.services = services;
  }
}
