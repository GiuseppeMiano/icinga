package it.unimore.monitor.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
=================================================================================================
Created on 07/11/2017
*
AuthenticationFilter.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class AuthenticationFilter implements Filter{

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {
    System.out.println("Run filter...");
    if (((HttpServletRequest) servletRequest).getSession().getAttribute("username") == null) {
      ((HttpServletResponse) servletResponse).sendRedirect("../Login.xhtml");
    } else {
      filterChain.doFilter(servletRequest, servletResponse);
    }
  }

  @Override
  public void destroy() {

  }
}
