package it.unimore.monitor.view.home.model;

import com.google.gson.reflect.TypeToken;
import it.unimore.monitor.client.configuration.CommandConfiguration;
import it.unimore.monitor.client.configuration.IConfiguration;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import it.unimore.monitor.client.http.Icinga2CreateRequest;
import it.unimore.monitor.client.http.Icinga2DeleteRequest;
import it.unimore.monitor.client.http.Icinga2EditRequest;
import it.unimore.monitor.client.http.Icinga2ListRequest;
import it.unimore.monitor.client.http.Icinga2RequestFactory;
import it.unimore.monitor.client.http.Icinga2Response;
import it.unimore.monitor.client.http.Icinga2SupportedAction;
import it.unimore.monitor.client.http.Icinga2SupportedRequestType;
import it.unimore.monitor.view.login.model.LoginPOJO;
import it.unimore.monitor.utils.BeanUtils;
import it.unimore.monitor.utils.IcingaUtils;
import it.unimore.monitor.utils.SerializationUtils;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/*
=================================================================================================
Created on 01/12/2017
*
HomeDAO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class HomeDAO implements Serializable{
  private final static Logger log = Logger.getLogger(HomeDAO.class);
  private List<CommandPOJO> commands;
  private List<NodePOJO> nodes;
  private NodePOJO node;
  private CommandPOJO command;
  private ServicePOJO service;
  private LoginPOJO login;
  private NewPOJO newElement;

  private Icinga2ListRequest listRequest;
  private Icinga2CreateRequest createRequest;
  private Icinga2DeleteRequest deleteRequest;
  private Icinga2EditRequest editRequest;

  private IConfiguration json;

  @PostConstruct
  public void init() {
    try {
      Icinga2RequestFactory factory = IcingaUtils.getIcingaRequestFactory(login);
      newElement = new NewPOJO();
      listRequest = (Icinga2ListRequest) factory.getRequest(Icinga2SupportedAction.LIST);
      createRequest = (Icinga2CreateRequest) factory.getRequest(Icinga2SupportedAction.CREATE);
      deleteRequest = (Icinga2DeleteRequest) factory.getRequest(Icinga2SupportedAction.DELETE);
      editRequest = (Icinga2EditRequest) factory.getRequest(Icinga2SupportedAction.EDIT);
      initNodes();
      initCommands();
    } catch (Exception e) {
      log.error(e);
      BeanUtils.printMessage("Init failed!", FacesMessage.SEVERITY_ERROR);
    }
  }

  public void reset() {
    this.command = null;
    this.node = null;
  }

  private void initNodes() throws Exception {
    log.info("User " + login.getUsername() + " is trying to get the list of nodes...");
    Icinga2Response icinga2Response = listRequest.listNodes();
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    this.nodes = SerializationUtils.getGson().fromJson(
        IcingaUtils.getResults(icinga2Response).toString(), new TypeToken<List<NodePOJO>>(){}.getType());
    log.info("...nodes fetched properly.");
  }

  private void initCommands() throws IOException {
    log.info("User " + login.getUsername() + " is trying to get the list of commands...");
    log.info("Getting check commands...");
    List<CommandPOJO> newCommands = new ArrayList<>();
    Icinga2Response icinga2Response = listRequest.listCheckCommands();
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    newCommands.addAll((Collection<? extends CommandPOJO>) SerializationUtils.getGson()
        .fromJson(IcingaUtils.getResults(icinga2Response).toString(), new TypeToken<List<CommandPOJO>>(){}.getType()));
    log.info("Getting event commands...");
    icinga2Response = listRequest.listEventCommands();
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    newCommands.addAll((Collection<? extends CommandPOJO>) SerializationUtils.getGson()
        .fromJson(IcingaUtils.getResults(icinga2Response).toString(), new TypeToken<List<CommandPOJO>>(){}.getType()));
    log.info("Getting notification commands...");
    icinga2Response = listRequest.listNotificationCommands();
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    newCommands.addAll((Collection<? extends CommandPOJO>) SerializationUtils.getGson()
        .fromJson(IcingaUtils.getResults(icinga2Response).toString(), new TypeToken<List<CommandPOJO>>(){}.getType()));
    this.commands = newCommands;
    log.info("...commands fetched properly.");
  }

  public void getSettings(String type) {
    log.info("Update settings of " + type + "...");
    try {
      switch (type) {
        case "node": getNodeSettings(); break;
        case "service": getServiceSettings(); break;
        case "command": getCommandSettings(); break;
        default: throw new RuntimeException("Object " + type + " behavior not coded.");
      }
    } catch (Exception e) {
      log.error(e);
      BeanUtils.printMessage("Get Settings for " + type + " failed!", FacesMessage.SEVERITY_ERROR);
    }
  }

  private void getNodeSettings() throws Exception {
    log.info("The node " + node.getName() + " was selected.");
    log.info("Update node attributes...");
    if (node.getAttrs() == null) {
      node.setAttrs(getNodeAttrs(node));
    }
    if (node.getServices() == null) {
      node.setServices(getNodeServices(node));
    }
    RequestContext.getCurrentInstance().addCallbackParam("json", SerializationUtils.serialize(node));
  }

  private NodeConfiguration getNodeAttrs(NodePOJO thisNode) throws IOException {
    // Get Node attributes
    Icinga2Response icinga2Response = listRequest.listNodeConfiguration(thisNode.getName());
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    NodeConfiguration config = SerializationUtils.deserialize(SerializationUtils
        .serialize(IcingaUtils.getResults(icinga2Response).get(0)
            .getAsJsonObject().get("attrs")), new TypeToken<NodeConfiguration>(){}.getType());
    return config;
  }

  private List<ServicePOJO> getNodeServices(NodePOJO thisNode) throws IOException {
    // Get Node services
    Icinga2Response icinga2Response = listRequest.listNodeServices(thisNode.getName());
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    List<ServicePOJO> services = SerializationUtils.getGson().fromJson(
        IcingaUtils.getResults(icinga2Response), new TypeToken<List<ServicePOJO>>(){}.getType());
    return services;
  }

  private void getCommandSettings() throws IOException {
    log.info("The node " + command.getName() + " was selected.");
    if (command.getAttrs() == null) {
      log.info("Update command attributes...");
      Icinga2SupportedRequestType commandType = Icinga2SupportedRequestType.fromString(command.getType() + "s");
      Icinga2Response icinga2Response = listRequest.listCommandConfiguration(command.getName(), commandType);
      if (icinga2Response.getResponseCode() != 200) {
        IcingaUtils.getResponse(icinga2Response);
      }
      CommandConfiguration config = SerializationUtils.deserialize(SerializationUtils
          .serialize(IcingaUtils.getResults(icinga2Response)
              .get(0).getAsJsonObject().get("attrs")), new TypeToken<CommandConfiguration>(){}.getType());
      command.setAttrs(config);
    }
    RequestContext.getCurrentInstance().addCallbackParam("json", SerializationUtils.serialize(command));
  }

  private void getServiceSettings() throws IOException {
    log.info("The service " + service.getName() + " of the node: " + node.getName() + "was selected.");
    if (service.getAttrs() == null) {
      log.info("Update service attributes...");
      // Get Node attributes
      Icinga2Response icinga2Response = listRequest.listNodeServiceConfiguration(service.getName());
      if (icinga2Response.getResponseCode() != 200) {
        IcingaUtils.getResponse(icinga2Response);
      }
      ServiceConfiguration config = SerializationUtils.deserialize(SerializationUtils
          .serialize(IcingaUtils.getResults(icinga2Response).get(0).getAsJsonObject()
              .get("attrs")), new TypeToken<ServiceConfiguration>(){}.getType());
      service.setAttrs(config);
    }
    RequestContext.getCurrentInstance().addCallbackParam("json", SerializationUtils.serialize(service));
  }

  public void create(String type) {
    log.info("Create a new " + type + ".");
    try {
      switch (type) {
        case "node": createNode(); break;
        case "command": createCommand(); break;
        case "service": createService(); break;
        default:
          throw new RuntimeException("Object " + type + " create behavior not coded.");
      }
      newElement.destroy();
      BeanUtils.printMessage("Element: " + type + " created.");
    } catch (Exception e) {
      log.error(e);
      BeanUtils.printMessage("Create for " + type + " failed!", FacesMessage.SEVERITY_ERROR);
    }
  }

  private void createNode() throws IOException {
    log.info("Registering a new node...");
    // Create the node
    Icinga2Response icinga2Response = createRequest.addNode(newElement.getName(), newElement.serializeNode());
    if(icinga2Response.getResponseCode() != 200) {
      log.error("An error occurred during node: " + newElement.getName() + " registration.");
      IcingaUtils.getResponse(icinga2Response);
    }
    else {
      // Add the node to the registered ones
      NodePOJO newNode = new NodePOJO();
      List<ServicePOJO> services = new ArrayList<>();
      newNode.setName(newElement.getName());
      // register all the node services
      ServicePOJO service;
      for (CommandPOJO command: newElement.getNodeServices()) {
        icinga2Response = createRequest.addServiceToNode(newElement.getName(),
            command.getName(), SerializationUtils.serializeNodeService(command));
        if(icinga2Response.getResponseCode() != 200) {
          IcingaUtils.getResponse(icinga2Response);
        }
        /*service = new ServicePOJO();
        service.setName(command.getName());
        services.add(service);*/
      }
      // Register all the services
      //newNode.setServices(services);
      nodes.add(newNode);
    }
    log.info("...node registration completed.");
  }

  private void createCommand() throws IOException {
    log.info("Registering a new command...");
    Icinga2Response icinga2Response = createRequest.addCommand(newElement.getName(),
        newElement.serializeCommand(), Icinga2SupportedRequestType.fromString(newElement.getCommandType() + "s"));
    if(icinga2Response.getResponseCode() != 200) {
      log.error("An error occurred during service: " + newElement.getName() + " registration.");
      IcingaUtils.getResponse(icinga2Response);
    }
    else {
      // Add command to the registered ones
      CommandPOJO command = new CommandPOJO();
      command.setName(newElement.getName());
      command.setType(newElement.getCommandType());
      commands.add(command);
    }
    log.info("..command registration completed.");
  }

  private void createService() throws IOException {
    log.info("Registering a new service...");
    Icinga2Response icinga2Response = createRequest.addServiceToNode(node.getName(), newElement.getName(), newElement.serializeService());
    if(icinga2Response.getResponseCode() != 200) {
      log.error("An error occurred during service: " + newElement.getName() + " registration in node " + node.getName() + ".");
      IcingaUtils.getResponse(icinga2Response);
    }
    else {
      // Add the service to the registered ones for node
      ServicePOJO service = new ServicePOJO();
      service.setName(node.getName() + "!" + newElement.getName());
      node.getServices().add(service);
    }
    log.info("..service registration completed.");
  }

  public void update(String type) {
    log.info("Update " + type + ".");
    log.info("The update json is: " + SerializationUtils.serialize(json));

    try{
      switch (type) {
        case "node": updateNode(); break;
        case "command": updateCommand(); break;
        case "service": updateService(); break;
        default: throw new RuntimeException("Object " + type + " create behavior not coded.");
      }
    } catch (Exception e) {
      log.error(e);
      BeanUtils.printMessage("Update for " + type + " failed!", FacesMessage.SEVERITY_ERROR);
    }
  }

  private void updateNode() throws Exception {
    log.info("Updating node " + node.getName() + "...");
    // Verify if JSON has changed
    log.info("The old json was: " + SerializationUtils.serialize(node));
    if (json.equals(node.getAttrs())) {
      log.info("Node: " + node.getName() + " configuration has not changed, do nothing.");
    } else {
      NodeConfiguration temp = node.getAttrs();
      node.setAttrs((NodeConfiguration) json);
      log.info("Actually update node: " + node.getName() + " configuration.");
      // Check for ok response
      Icinga2Response icinga2Response = editRequest.editNode(node.getName(),
          (NodeConfiguration) json);
      if (icinga2Response.getResponseCode() != 200) {
        node.setAttrs(temp);
        IcingaUtils.getResponse(icinga2Response);
      }
    }
    log.info("..node update completed.");
  }

  private void updateService() throws IOException {
    log.info("Updating service " + service.getName() + "...");
    // Verify if JSON has changed
    log.info("The old json was: " + SerializationUtils.serialize(service));
    if (json.equals(service.getAttrs())) {
      log.info("Service: " + service.getName() + " configuration has not changed, do nothing.");
    } else {
      ServiceConfiguration temp = service.getAttrs();
      service.setAttrs((ServiceConfiguration) json);
      log.info("Actually update service: " + service.getName() + " configuration.");
      // Check for ok response
      Icinga2Response icinga2Response = editRequest.editNodeService(node.getName(), service.getAttrs().getName(),
          (ServiceConfiguration) json);
      if (icinga2Response.getResponseCode() != 200) {
        service.setAttrs(temp);
        IcingaUtils.getResponse(icinga2Response);
      }
    }
    log.info("..service update completed.");
  }

  private void updateCommand() throws IOException {
    log.info("Updating command " + command.getName() + "...");
    // Verify if JSON has changed
    log.info("The old json was: " + SerializationUtils.serialize(command));
    if (json.equals(command.getAttrs())) {
      log.info("Command: " + command.getName() + " configuration has not changed, do nothing.");
    } else {
      CommandConfiguration temp = command.getAttrs();
      command.setAttrs((CommandConfiguration) json);
      log.info("Actually update command: " + command.getName() + " configuration.");
      Icinga2SupportedRequestType commandType = Icinga2SupportedRequestType.fromString(command.getType() + "s");
      // Check for ok response
      Icinga2Response icinga2Response = editRequest.editCommand(command.getName(), command.getAttrs(), commandType);
      if (icinga2Response.getResponseCode() != 200) {
        command.setAttrs(temp);
        IcingaUtils.getResponse(icinga2Response);
      }
    }
    log.info("...command update completed.");
  }

  public void delete(String type) {
    log.info("Delete " + type + ".");
    try {
      switch (type) {
        case "node": deleteNode(); break;
        case "command": deleteCommand(); break;
        case "service": deleteService(); break;
        default: throw new RuntimeException("Object " + type + " create behavior not coded.");
      }
    } catch (Exception e) {
      log.error(e);
      BeanUtils.printMessage("Delete for " + type + " failed!", FacesMessage.SEVERITY_ERROR);
    }
  }

  private void deleteNode() throws Exception {
    log.info("Deleting node " + node.getName() + "...");
    Icinga2Response icinga2Response = deleteRequest.removeNode(node.getName());
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    // Delete the node from the list
    this.nodes.remove(this.node);
    // Set to null this object
    this.node = null;
    this.service = null;
    log.info("..node delete completed.");
  }

  private void deleteService() throws IOException {
    log.info("Deleting service " + service.getName() + "...");
    Icinga2Response icinga2Response = deleteRequest.removeServiceFromNode(node.getName(), service.getAttrs().getName());
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    node.getServices().remove(service);
    this.service = null;
    log.info("..service delete completed.");
  }

  private void deleteCommand() throws Exception {
    log.info("Deleting command " + command.getName() + "...");
    Icinga2SupportedRequestType commandType = Icinga2SupportedRequestType.fromString(command.getType() + "s");
    Icinga2Response icinga2Response = deleteRequest.removeCommand(command.getName(), commandType);
    if (icinga2Response.getResponseCode() != 200) {
      IcingaUtils.getResponse(icinga2Response);
    }
    commands.remove(command);
    this.command = null;
    this.service = null;
    // Update nodes services
    /* TODO: find a more efficient solution*/
    for (NodePOJO node: nodes) {
      node.setServices(getNodeServices(node));
    }
    log.info("...command delete completed.");
  }

  public List<CommandPOJO> getCommands() {
    return commands;
  }

  public void setLogin(LoginPOJO login) {
    this.login = login;
  }

  public CommandPOJO getCommand() {
    return command;
  }

  public void setCommand(CommandPOJO command) {
    this.command = command;
  }

  public List<NodePOJO> getNodes() {
    return nodes;
  }

  public NodePOJO getNode() {
    return node;
  }

  public void setNode(NodePOJO node) {
    this.node = node;
  }

  public ServicePOJO getService() {
    return service;
  }

  public void setService(ServicePOJO service) {
    this.service = service;
  }

  public NewPOJO getNewElement() {
    return newElement;
  }

  public void setNewElement(NewPOJO newElement) {
    this.newElement = newElement;
  }

  public IConfiguration getJson() {
    return json;
  }

  public void setJson() {
    Map map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    String json = (String) map.get("json");
    String elementType = (String) map.get("type");
    Type type;
    switch (elementType) {
      case "node": type = new TypeToken<NodeConfiguration>(){}.getType(); break;
      case "service": type = new TypeToken<ServiceConfiguration>(){}.getType(); break;
      case "command": type = new TypeToken<CommandConfiguration>(){}.getType(); break;
      default: throw new RuntimeException("Object " + elementType + " create behavior not coded.");
    }
    this.json = SerializationUtils.deserialize(json, type);
  }
}
