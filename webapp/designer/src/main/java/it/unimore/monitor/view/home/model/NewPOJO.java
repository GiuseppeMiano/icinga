package it.unimore.monitor.view.home.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import it.unimore.monitor.client.configuration.CommandConfiguration;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import it.unimore.monitor.utils.SerializationUtils;
import java.util.List;
import java.util.ListIterator;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 08/12/2017
*
NewPOJO.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class NewPOJO {
  private final static Logger log = Logger.getLogger(HomeDAO.class);
  private String name;
  private String nodeAddress;
  private String nodeOs;
  private List<CommandPOJO> nodeServices;
  private CommandPOJO serviceCommand;
  private String commandBinsList;
  private String commandArgsList;
  private String commandType;

  public void destroy() {
    name = null;
    nodeAddress = null;
    nodeOs = null;
    nodeServices = null;
    serviceCommand = null;
    commandType = null;
    commandBinsList = null;
    commandArgsList = null;
  }

  public NodeConfiguration serializeNode() {
    log.info("START serializing a node configuration...");
    NodeConfiguration config = new NodeConfiguration();
    config.setAddress(nodeAddress);
    JsonObject vars = new JsonObject();
    vars.addProperty("os", nodeOs);
    config.setVars(vars);
    // Override default service
    if (nodeServices != null) {
      ListIterator<CommandPOJO> servicesIterator = nodeServices.listIterator();
      CommandPOJO command;
      while (servicesIterator.hasNext() && (config.getEventCommand() == null || config.getCheckCommand() == null)) {
        command = servicesIterator.next();
        if (command.getType().equals("CheckCommand")) {
          config.setCheckCommand(command.getName());
        }
        if (command.getType().equals("EventCommand")) {
          config.setEventCommand(command.getName());
        }
      }
    }
    log.info("Serialized configuration is: " + SerializationUtils.serialize(config) + ".");
    log.info("...node configuration serialization END.");
    return config;
  }

  public ServiceConfiguration serializeService () {
    log.info("START serializing a service configuration...");
    ServiceConfiguration config = new ServiceConfiguration();
    switch (serviceCommand.getType()) {
      case "CheckCommand": config.setCheckCommand(serviceCommand.getName()); break;
      case "EventCommand": config.setEventCommand(serviceCommand.getName()); break;
      default: throw new RuntimeException("Error with command type during serialization!");
    }
    log.info("Serialized configuration is: " + SerializationUtils.serialize(config) + ".");
    log.info("...service configuration serialization END.");
    return config;
  }

  public CommandConfiguration serializeCommand () {
    log.info("START serializing a command configuration...");
    CommandConfiguration config = new CommandConfiguration();
    if (commandBinsList != null) {
      config.setCommand(commaSeparatedListToArray(commandBinsList));
    }
    log.info("Serialized configuration is: " + SerializationUtils.serialize(config) + ".");
    log.info("...node configuration serialization END.");
    return config;
  }

  public JsonArray commaSeparatedListToArray (String list) {
    String[] arrayList = list.split(",");
    JsonArray jsonArray = new JsonArray();
    for (String el: arrayList) {
      jsonArray.add(el.trim());
    }
    return jsonArray;
  }

  public JsonObject commaSeparatedKeyPairsToDictionary (String list) {
    String[] arrayList = list.split(",");
    JsonObject jsonObject = new JsonObject();
    String[] keyPair;
    for (String el: arrayList) {
      keyPair = el.split(" ");
      if (keyPair.length != 2) {
        log.warn("Element: " + el + " is not well formed and will be ignored.");
        continue;
      }
      jsonObject.addProperty(keyPair[0].trim(), keyPair[1].trim());
    }
    return jsonObject;
  }

  public String getNodeAddress() {
    return nodeAddress;
  }

  public void setNodeAddress(String nodeAddress) {
    this.nodeAddress = nodeAddress;
  }

  public String getNodeOs() {
    return nodeOs;
  }

  public void setNodeOs(String nodeOs) {
    this.nodeOs = nodeOs;
  }

  public List<CommandPOJO> getNodeServices() {
    return nodeServices;
  }

  public void setNodeServices(List<CommandPOJO> nodeServices) {
    this.nodeServices = nodeServices;
  }

  public CommandPOJO getServiceCommand() {
    return serviceCommand;
  }

  public void setServiceCommand(CommandPOJO serviceCommand) {
    this.serviceCommand = serviceCommand;
  }

  public String getName() {
    return name;
  }

  public void setName(String elementName) {
    this.name = elementName;
  }

  public String getCommandType() {
    return commandType;
  }

  public void setCommandType(String commandType) {
    this.commandType = commandType;
  }

  public String getCommandBinsList() {
    return commandBinsList;
  }

  public void setCommandBinsList(String commandBinList) {
    this.commandBinsList = commandBinList;
  }

  public String getCommandArgsList() {
    return commandArgsList;
  }

  public void setCommandArgsList(String commandArgsList) {
    this.commandArgsList = commandArgsList;
  }
}