var editorsSettings = {
    "nodesContent": {
      "options": {
        modes: ['text', 'code', 'tree'],
        mode: 'tree'
      }
    },
    "commandsContent": {
      "options": {
        modes: ['text', 'code', 'tree'],
        mode: 'tree'
      }
    },
  "servicesContent": {
    "options": {
      modes: ['text', 'code', 'tree'],
      mode: 'tree'
    }
  }
};
$(document).ready(function () {
  initEditor();
  $('.nav-bar li a').click(function(e) {
    if (!$(this).attr('href').startsWith("#")) {
      windw.open($(this).attr('href'))
    } else {
      $('#container ').find('.table').hide(); // hides all content divs
      $('.nav-bar li.active').removeClass('active');
      var $parent = $(this).parent();
      $parent.addClass('active');
      e.preventDefault();
      //var url = this.href;
      //$("#container").load(url);
      /*if ($(this).attr('id').indexOf('node') < 0) {
        $('#serviceTabSelector').hide();
      }*/
      $($(this).attr('href')).show(); //get the href and use it find which div to show
    }
  });
  $('.nav-bar li.active a').trigger('click');
});

function initEditor () {
  var divs
      , index
      , id
      , editor;

  divs = $(".editor");
  for (index = 0 ; index < divs.length; ++index) {
    id = divs[index].id;
    editor = new JSONEditor(divs[index], editorsSettings[id].options);
    editorsSettings[id].editor = editor;
  }
}

function updateNodes (json) {
  console.log("Update node editor with selected node settings...");
  $('#serviceTabSelector').parent().show();
  updateEditor(editorsSettings.nodesContent.editor, json);
  updateEditor(editorsSettings.servicesContent.editor, "{}");
}

function updateCommands(json) {
  console.log("Update command editor with selected node settings...");
  updateEditor(editorsSettings.commandsContent.editor, json);
}

function updateServices(json) {
  console.log("Update service editor with selected node settings...");
  updateEditor(editorsSettings.servicesContent.editor, json);
}

function nodeDeleted() {
  console.log("Update service and node editors with empty json...");
  $('#serviceTabSelector').parent().hide();
  updateEditor(editorsSettings.nodesContent.editor, "{}");
  updateEditor(editorsSettings.servicesContent.editor, "{}");
}

function serviceDeleted() {
  console.log("Update service editors with empty json...");
  updateEditor(editorsSettings.servicesContent.editor, "{}");
}

function commandDeleted() {
  console.log("Update command editor with empty json...");
  updateEditor(editorsSettings.commandsContent.editor, "{}");
  updateEditor(editorsSettings.servicesContent.editor, "{}")
}

function getJson(type) {
  var json;
  switch (type) {
    case 'node': json = editorsSettings.nodesContent.editor.get(); break;
    case 'service': json = editorsSettings.servicesContent.editor.get(); break;
    case 'command': json = editorsSettings.commandsContent.editor.get(); break;
    default: throw new RangeException();
  }
  json = JSON.stringify(json.attrs);
  getJsonInJSF([{name: "json", value: json}, {name: "type", value: type}]);
}

function updateEditor(editor, json) {
  json = json || "{}";
  editor.setText(json);
}
