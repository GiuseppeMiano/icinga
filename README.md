# Project Description
This repository contains all the artifact required to build and run a monitoring system with the following requirements:
- Icinga 2 as back-end server
- A WAS a middle-tier server (JBoss AS v7.1.1)
- Use of J2EE
- MVC paradigm
- Java Beans implemented through java-server-faces

The application offers a Web UI to manage your Icinga Server configuration.

## Requirements
In order to use the infrastructure it is required to have:
- Java 7 installed (click [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) to download the software);
- Maven 3 installed (click [here](https://maven.apache.org/download.cgi) to download the software) and configured to use Java 8;
- An Icinga Server up and running;
- A bash command line for setup;
- A JBoss AS (the project is tested with version 7.1.1);

## Artifacts
The artifacts produced during the packaging are:
 1. A jar archive, that is the Icinga client;
 2. A war archive, installable in a WAS;

## How to use
Here are described all the features offered by maven during development.
### Compile
The project is compiled through maven version 3. All the artifacts can be built in a whole by running from this
folder the command:

```
 $ mvn clean package
``` 

### Deploy
In order to deploy the ear artifact within your JBoss AS it is possible to use maven to automate the procedure.
1. Configure the file _./ear/pom.xml_ and specify JBoss *hostname, username and password*;
2. Tunnel to access remote port 9999 with the command
   ```
    $ ssh -L 9999:localhost:9999 centos@<host-ip>
   ```
3. Run maven installation with the command
   ```
    $ mvn clean install
   ```
 
## Tips
It is possible to use the Virtual Box VM available [here](https://drive.google.com/open?id=10odljw7X3S40NznqEWY7akSwsHJzD56M)
that is already configured and ready to be used with the following softwares:
1. An Icinga Server;
2. A JBoss Application Server; 
 
## Style Guide
 
Before adding new classes or making any changes to the code, the developer should configure their own IDE to use the Google Code Style which can be found on the following repository:

- https://github.com/google/styleguide

For IntelliJ users, you can follow this guide : https://www.jetbrains.com/help/idea/2016.3/configuring-code-style.html.
For Eclipse user, you can refer to this guide : http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_c_code_style_pref.htm
 
You'll also need to modify the File Header of the Class template, to add the following header to the newly created classes: 

```
/** 
=================================================================================================
Created on ${DATE}
Copyright (C) 2017 Doxee S.p.A. C.F. - P.IVA: IT02714390362. All Rights Reserved
*
${NAME}.java
*
Comments are welcome.
*
Author: ${USER} <${USER}@doxee.com>
 
=================================================================================================
*/
 
```