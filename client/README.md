# Project Description
This repository contains a client API through which it is possible to interact with an Icinga
server in order to:
- Manage nodes;
- Manage services;

## Requirements
In order to manage the project correctly it is required to have:
- Java 7 installed (click [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) to download the software);
- Maven 3 installed (click [here](https://maven.apache.org/download.cgi) to download the software) and configured to use Java 8;
- An Icinga Server up and running;
- A bash command line for setup;

## Artifacts
This module's packaging produces:
 1. A JAR archive _./target/target/client-<version>-jar_;

## How to use
See root folder *REAMDE.md* for an insight.

## Setup
HTTPS protocol is used in order to authenticate requests through SSL. Your server can be configured
either to use a CA certificate or a Self-signed certificate. In the second case it is necessary to 
setup the Java VM by adding your certificate; otherwise requests return an Exception.

In order to configure the environment run the following instructions:

1. Get server certificate (Note to replace parameters between <>):

   ```
      echo -n | openssl s_client -connect <icinga-ip>:<icinga-port> | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/server.crt
   ```
2. Get certificate allowed CNAME from the certificate:
   ````
      keytool -printcert -v -file /tmp/server.crt
   ``` 
3. Note down the value of the property SubjectAlternativeName[0].DNSName (e.g. localhost.localdomain)
4. Add within _/etc/hosts_ (in Windows _C:\Windows\System32\drivers\etc\hosts_) the line:
   ```
    192.168.56.105 localhost.localdomain
   ```
5. Add the certificate downloaded in the step *1* to your java cacerts;
   ```
   ######################
   # On Windows Systems 
   ######################
   "%JAVA_HOME%\bin\keytool.exe" -import -trustcacerts -keystore "%JAVA_HOME%\jre\lib\security\cacerts" -storepass changeit -noprompt -alias icinga -file C:\server.crt
   ###################
   # On Unix Systems 
   ###################
   keytool -import -trustcacerts -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt -alias icinga -file /tmp/server.crt
   ```

