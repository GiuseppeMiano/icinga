package it.unimore.monitor.client.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.google.gson.reflect.TypeToken;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import java.io.IOException;
import java.util.Date;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
=================================================================================================
Created on 02/11/2017
*
Icinga2EditRequestIT.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2EditRequestIT extends Icinga2RequestBaseIT {
  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);
  public Icinga2EditRequest editRequest;

  @Before
  public void setUp() throws Exception {
    init();
    // Create Create requests
    editRequest = (Icinga2EditRequest) factory.getRequest(Icinga2SupportedAction.EDIT);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void editNode() throws IOException {
    assertNotNull(props.getProperty("nodeName"));
    NodeConfiguration json = gson.fromJson("{\"vars\": {\"version\": \"" + new Date().getTime() + "\"}}",
        new TypeToken<NodeConfiguration>(){}.getType());
    log.info("Send the configuration: " + json + " to modify a node.");
    Icinga2Response response = editRequest.editNode(props.getProperty("nodeName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void editService() throws IOException {
    assertNotNull(props.getProperty("serviceName"));
    ServiceConfiguration json = gson.fromJson("{\"vars\": {\"version\": \"" + new Date().getTime() + "\"}}",
        new TypeToken<ServiceConfiguration>(){}.getType());
    log.info("Send the configuration: " + json + " to modify a node.");
    Icinga2Response response = editRequest.editNodeService(props.getProperty("nodeName"), props.getProperty("serviceName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }
}
