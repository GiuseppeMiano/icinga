package it.unimore.monitor.client.http;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import java.util.Properties;
import org.apache.http.auth.UsernamePasswordCredentials;

/*
=================================================================================================
Created on 01/11/2017
*
Icinga2RequestBaseIT.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2RequestBaseIT {
  static Properties props;
  static Icinga2RequestFactory factory;
  static JsonParser parser = new JsonParser();
  static Gson gson = new Gson();

  public static void init() throws Exception {
    // Read test properties
    props = new Properties();
    props.load(Icinga2RequestBaseIT.class.getResourceAsStream("/config.properties"));
    // Create the builder
    Icinga2ClientBuilder builder = Icinga2ClientBuilder.create();
    // Setup Available properties
    if (props.getProperty("hostname") != null) {
      builder.withHostname(props.getProperty("hostname"));
    }
    if (props.getProperty("port") != null) {
      builder.withPort(Integer.parseInt(props.getProperty("port")));
    }
    if (props.getProperty("username") != null && props.getProperty("password") != null) {
      builder.withCredentials(props.getProperty("username"), props.getProperty("password"));
    }
    // Build the factory
    factory = new Icinga2RequestFactory(builder.build());
  }

  public void destroy() throws Exception {
  }

}
