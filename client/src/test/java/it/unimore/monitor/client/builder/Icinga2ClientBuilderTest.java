package it.unimore.monitor.client.builder;

import static org.junit.Assert.*;

import it.unimore.monitor.client.Icinga2;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import java.io.IOException;
import java.net.MalformedURLException;
import org.junit.Test;

public class Icinga2ClientBuilderTest {

  @Test
  public void testDefaultClientBuilder () throws IOException {
    // Initialize the client. It can raise a Connection Exception if no process is listening
    // on the default port is not listening
    Icinga2 client = Icinga2ClientBuilder.defaultClient();
    assertNotNull("The client was initialized and a service is listening here: "
          + client.getUrl(), client);
  }

  @Test
  public void testCustomClientBuilder () throws IOException {
    Icinga2 client = Icinga2ClientBuilder
        .create()
        .withHostname("google.it")
        .withPort(443)
        .withCredentials("test", "password")
        .build();
    assertNotNull("The custom client was initialized and a service is listening here: "
        + client.getUrl(), client);
  }

  @Test (expected = MalformedURLException.class)
  public void testInvalidUrlException () throws IOException {
    Icinga2 client = Icinga2ClientBuilder
        .create()
        .withHostname("http://google.it")
        .withPort(443)
        .withCredentials("test", "password")
        .build();
    assertNotNull("The custom client was initialized and a service is listening here: "
        + client.getUrl(), client);
  }
}