package it.unimore.monitor.client.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Icinga2DeleteRequestIT extends Icinga2RequestBaseIT {
  private final static Logger log = Logger.getLogger(Icinga2DeleteRequestIT.class);
  public Icinga2DeleteRequest deleteRequest;

  @Before
  public void setUp() throws Exception {
    init();
    // Create Delete requests
    deleteRequest = (Icinga2DeleteRequest) factory.getRequest(Icinga2SupportedAction.DELETE);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void firstRemoveServiceFromNode() throws IOException {
    assertNotNull(props.getProperty("serviceName"));
    assertNotNull(props.getProperty("nodeName"));
    Icinga2Response response = deleteRequest.removeServiceFromNode(props.getProperty("nodeName"), props.getProperty("serviceName"));
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void secondRemoveNode() throws Exception {
    assertNotNull(props.getProperty("nodeName"));
    Icinga2Response response = deleteRequest.removeNode(props.getProperty("nodeName"));
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void removeCheckCommand() throws IOException {
    assertNotNull(props.getProperty("checkCommandName"));
    Icinga2Response response = deleteRequest.removeCheckCommand(props.getProperty("checkCommandName"));
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void removeEventCommand() throws IOException {
    assertNotNull(props.getProperty("eventCommandName"));
    Icinga2Response response = deleteRequest.removeEventCommand(props.getProperty("eventCommandName"));
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void removeNotificationCommand() throws IOException {
    assertNotNull(props.getProperty("notificationCommandName"));
    Icinga2Response response = deleteRequest.removeNotificationCommand(props.getProperty("notificationCommandName"));
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }
}