package it.unimore.monitor.client.http;
import static org.junit.Assert.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Icinga2ListRequestIT extends Icinga2RequestBaseIT {

  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);
  public Icinga2ListRequest listRequest;

  @Before
  public void setUp() throws Exception {
    init();
    // Create List requests
    listRequest = (Icinga2ListRequest) factory.getRequest(Icinga2SupportedAction.LIST);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void listNodes() throws Exception {
    Icinga2Response response = listRequest.listNodes();
    log.info(response.getResponsePayload());
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void listNodeConfiguration() throws IOException {
    assertNotNull(props.getProperty("hostname"));
    Icinga2Response response = listRequest.listNodeConfiguration(props.getProperty("hostname"));
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Services returned correctly.", responseMessage.contains("Host")
        || responseMessage.contains("\"results\":[]"));
  }

  @Test
  public void listNodesServices() throws Exception {
    Icinga2Response response = listRequest.listNodesServices();
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Services returned correctly.", responseMessage.contains("Service")
        || responseMessage.contains("\"results\":[]"));
  }

  @Test
  public void listNodeServices() throws Exception {
    assertNotNull(props.getProperty("hostname"));
    Icinga2Response response = listRequest.listNodeServices(props.getProperty("hostname"));
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Services returned correctly.", responseMessage.contains("Service")
        || responseMessage.contains("\"results\":[]"));
  }

  @Test
  public void listNodeServiceConfiguration() throws Exception {
    assertNotNull(props.getProperty("hostname"));
    String serviceName = "ping4";
    Icinga2Response response = listRequest.listNodeServiceConfiguration(props.getProperty("hostname"), serviceName);
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    JsonArray obj = new JsonParser().parse(responseMessage).getAsJsonObject().getAsJsonArray("results");
    assertTrue(obj.size() == 1);
  }

  @Test
  public void listCheckCommands() throws Exception {
    Icinga2Response response = listRequest.listCheckCommands();
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Check commands returned correctly.", responseMessage.contains("CheckCommand")
        || responseMessage.contains("\"results\":[]"));
  }

  @Test
  public void listNotificationCommands() throws Exception {
    Icinga2Response response = listRequest.listNotificationCommands();
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Notification commands returned correctly.", responseMessage.contains("NotificationCommand")
        || responseMessage.contains("\"results\":[]"));
  }

  @Test
  public void listEventCommands() throws Exception {
    Icinga2Response response = listRequest.listEventCommands();
    String responseMessage = response.getResponsePayload();
    log.info(responseMessage);
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
    assertTrue("Event commands returned correctly.", responseMessage.contains("EventCommand")
        || responseMessage.contains("\"results\":[]"));
  }
}