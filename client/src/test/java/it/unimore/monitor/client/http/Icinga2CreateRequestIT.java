package it.unimore.monitor.client.http;
import static org.junit.Assert.*;

import com.google.gson.reflect.TypeToken;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import it.unimore.monitor.client.configuration.CommandConfiguration;
import it.unimore.monitor.client.configuration.IConfiguration;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Icinga2CreateRequestIT extends Icinga2RequestBaseIT {
  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);
  public Icinga2CreateRequest createRequest;

  @Before
  public void setUp() throws Exception {
    init();
    // Create Create requests
    createRequest = (Icinga2CreateRequest) factory.getRequest(Icinga2SupportedAction.CREATE);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void firstAddNode() throws Exception {
    assertNotNull(props.getProperty("nodeConfig"));
    assertNotNull(props.getProperty("nodeName"));
    NodeConfiguration json = gson.fromJson(props.getProperty("nodeConfig"),
        new TypeToken<NodeConfiguration>(){}.getType());
    Icinga2Response response = createRequest.addNode(props.getProperty("nodeName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void secondAddServiceToNode() throws IOException {
    assertNotNull(props.getProperty("serviceNodeConfig"));
    assertNotNull(props.getProperty("serviceName"));
    assertNotNull(props.getProperty("nodeName"));
    ServiceConfiguration json = gson.fromJson(props.getProperty("serviceNodeConfig"),
        new TypeToken<ServiceConfiguration>(){}.getType());
    Icinga2Response response = createRequest.addServiceToNode(props.getProperty("nodeName"),
        props.getProperty("serviceName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void addEventCommand() throws IOException {
    assertNotNull(props.getProperty("commandConfig"));
    assertNotNull(props.getProperty("eventCommandName"));
    CommandConfiguration json = gson.fromJson(props.getProperty("commandConfig"),
        new TypeToken<CommandConfiguration>(){}.getType());
    Icinga2Response response = createRequest.addEventCommand(props.getProperty("eventCommandName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void addNotificationCommand() throws IOException {
    assertNotNull(props.getProperty("commandConfig"));
    assertNotNull(props.getProperty("notificationCommandName"));
    CommandConfiguration json = gson.fromJson(props.getProperty("commandConfig"),
        new TypeToken<CommandConfiguration>(){}.getType());
    Icinga2Response response = createRequest.addNotificationCommand(props.getProperty("notificationCommandName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }

  @Test
  public void addCheckCommand() throws IOException {
    assertNotNull(props.getProperty("commandConfig"));
    assertNotNull(props.getProperty("checkCommandName"));
    CommandConfiguration json = gson.fromJson(props.getProperty("commandConfig"),
        new TypeToken<CommandConfiguration>(){}.getType());
    Icinga2Response response = createRequest.addCheckCommand(props.getProperty("checkCommandName"), json);
    if (response.getResponseCode() != 200) {
      // Print error
      log.error("An error occurred server-side.");
      log.error("The server response is: " + response.getResponsePayload());
    }
    assertEquals("A OK response code was received.", 200, response.getResponseCode());
  }
}