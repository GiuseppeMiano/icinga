package it.unimore.monitor.client.http;

/*
=================================================================================================
Created on 03/11/2017
*
Icinga2RequestSuiteIT.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)

@SuiteClasses({
    Icinga2CreateRequestIT.class,
    Icinga2EditRequestIT.class,
    Icinga2DeleteRequestIT.class,
    Icinga2ListRequestIT.class
})
public class Icinga2RequestSuiteIT {

}
