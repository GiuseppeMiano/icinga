package it.unimore.monitor.client.configuration;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

/*
=================================================================================================
Created on 10/12/2017
*
CommandConfiguration.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class CommandConfiguration  implements IConfiguration{
  private JsonArray command = new JsonArray();
  private JsonObject env = new JsonObject();
  private JsonObject vars = new JsonObject();
  private Integer timeout = 60;
  private JsonObject arguments = new JsonObject();

  public JsonArray getCommand() {
    return command;
  }

  public void setCommand(JsonArray command) {
    this.command = command;
  }

  public JsonObject getEnv() {
    return env;
  }

  public void setEnv(JsonObject env) {
    this.env = env;
  }

  public JsonObject getVars() {
    return vars;
  }

  public void setVars(JsonObject vars) {
    this.vars = vars;
  }

  public int getTimeout() {
    return timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  public JsonObject getArguments() {
    return arguments;
  }

  public void setArguments(JsonObject arguments) {
    this.arguments = arguments;
  }
}
