package it.unimore.monitor.client.http;

import com.google.gson.JsonArray;
import it.unimore.monitor.client.Icinga2;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import it.unimore.monitor.client.configuration.CommandConfiguration;
import it.unimore.monitor.client.configuration.IConfiguration;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 02/11/2017
*
Icinga2EditRequest.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2EditRequest extends Icinga2RequestBase {
  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);

  Icinga2EditRequest(Icinga2 server, CloseableHttpClient httpClient) {
    super(server, httpClient);
  }

  Icinga2Response sendRequest(String url, IConfiguration attrs) throws IOException {
    HttpPost req = new HttpPost(url);
    log.info("The url to use is: " + url);
    // Set Required header
    req.addHeader("Accept", "application/json");
    // Set Request attrs
    req.setEntity(createExclusionPayload(attrs));
    log.info("Sending the request...");
    CloseableHttpResponse response = httpClient.execute(req);
    log.info("...request completed.");
    log.info("The received object is: " + response.toString());
    return new Icinga2Response(response);
  }

  /**
   * This method is used to edit the configuration of a node registered within Icinga.
   *
   * @param nodeName      host to modify the configuration to.
   * @param attrs       new configuration parameters of the host
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response editNode(String nodeName, NodeConfiguration attrs) throws IOException {
    log.info("Prepare a POST request to edit the node: " + nodeName + "configuration.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.HOST,
        nodeName);
    return sendRequest(url, attrs);
  }

  /**
   * This method is used to edit a service registered to a node within Icinga.
   *
   * @param nodeName      host to modify the service configuration to.
   * @param serviceName   service to modify the configuration to.
   * @param attrs       new configuration parameters of the registered service
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response editNodeService(String nodeName, String serviceName, ServiceConfiguration attrs)
      throws IOException {
    log.info("Prepare a POST request to edit the service: " + serviceName + "configuration for "
        + "node: " + nodeName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.SERVICE,
        nodeName + "!" + serviceName);
    return sendRequest(url, attrs);
  }

  /**
   * This function add a command for a given type.
   *
   * @param commandName     to fetch configuration of
   * @param attrs           configuration of the new command
   * @param type            Icinga2SupportedRequestType string
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response editCommand(String commandName, CommandConfiguration attrs, Icinga2SupportedRequestType type)
      throws IOException {
    log.info("Prepare a PUT request to add the command: " + commandName + ".");
    // Default templates
    JsonArray templates = new JsonArray();
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, attrs);
  }

  /**
   * This method is used to edit a check command registered within Icinga.
   *
   * @param commandName   command to modify the configuration to.
   * @param attrs       new configuration parameters of the registered command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response editCheckCommand(String commandName, CommandConfiguration attrs) throws IOException {
    log.info("Prepare a POST request to edit the command: " + commandName + "configuration.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.CHECK,
        commandName);
    return sendRequest(url, attrs);
  }

  /**
   * This method is used to edit a notification command registered within Icinga.
   *
   * @param commandName   command to modify the configuration to.
   * @param attrs       new configuration parameters of the registered command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response editNotificationCommand(String commandName, CommandConfiguration attrs) throws IOException {
    log.info("Prepare a POST request to edit the command: " + commandName + "configuration.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.NOTIFY,
        commandName);
    return sendRequest(url, attrs);
  }

  /**
   * This method is used to edit a event command registered within Icinga.
   *
   * @param commandName   command to modify the configuration to.
   * @param attrs       new configuration parameters of the registered command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response editEventCommand(String commandName, CommandConfiguration attrs) throws IOException {
    log.info("Prepare a POST request to edit the command: " + commandName + "configuration.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.EVENT,
        commandName);
    return sendRequest(url, attrs);
  }
}
