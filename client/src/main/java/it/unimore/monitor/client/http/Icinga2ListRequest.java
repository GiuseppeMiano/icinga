package it.unimore.monitor.client.http;

import it.unimore.monitor.client.Icinga2;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 29/10/2017
*
Icinga2ListRequest.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2ListRequest extends Icinga2RequestBase {

  private final static Logger log = Logger.getLogger(Icinga2ListRequest.class);
  private final static String NAME_QUERY_STRING = "name";
  private final static String SERVICE_BY_HOST_QUERY_STRING = "service.host_name==";

  /**
   * Base constructor runs parent constructor
   * @param server      Icinga2 correctly built client
   * @param httpClient  HttpClient, required to send requests
   */
  public Icinga2ListRequest(Icinga2 server, CloseableHttpClient httpClient) {
    super(server, httpClient);
  }

  Icinga2Response sendRequest(String url, String[] attrs, String[] filter, String[] joins, String[] meta)
      throws IOException {
    StringBuilder qS = new StringBuilder("?");
    if (attrs != null && attrs.length >= 1) {
      for (String nextAttr: attrs) {
        qS.append("attrs=").append(nextAttr).append("&");
      }
    }
    if (joins != null && joins.length >= 1) {
      for (String nextJoin: joins) {
        qS.append("joins=").append(nextJoin).append("&");
      }
    }
    if (meta != null && meta.length >= 1) {
      for (String nextMeta: meta) {
        qS.append("meta=").append(nextMeta).append("&");
      }
    }
    if (filter != null && filter.length >= 1) {
      for (String nextFilter: filter) {
        qS.append("filter=").append(nextFilter).append("&");
      }
    }
    url += (qS.toString().equals("?") ? "" : qS.deleteCharAt(qS.length() - 1).toString());
    HttpGet req = new HttpGet(url);
    log.info("The url to use is: " + url);
    // Set Required header
    req.addHeader("Accept", "application/json");
    log.info("Sending the request...");
    CloseableHttpResponse response = httpClient.execute(req);
    log.info("...request completed.");
    log.info("The received object is: " + response.toString());
    return new Icinga2Response(response);
  }

  /**
   * This function tries to authenticate to Icinga Server
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws IOException
   */
  public Icinga2Response authenticate() throws IOException {
    log.info("Prepare a GET request to verify authentication.");
    // Create the URL
    String url = server.getUrl();
    return sendRequest(url, null, null, null, null);
  }

  /**
   * This function lists all the nodes registered within Icinga 2 Monitor.
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodes() throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.HOST);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, null, null, null);
  }

  /**
   * This function lists all the settings of a specific node. The method overrides default get to use a filter
   * with a post request
   *
   * @param nodeName        to fetch configuration of
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodeConfiguration(String nodeName) throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.HOST, nodeName);
    return sendRequest(url, null, null, null, null);
  }

  /**
   * This function lists all the settings of a specific node.
   *
   * @param commandName     to fetch configuration of
   * @param type            Icinga2SupportedRequestType string
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listCommandConfiguration(String commandName, Icinga2SupportedRequestType type)
      throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, null, null, null, null);
  }

  /**
   * This function lists all the check commands registered within Icinga 2 Monitor.
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listCheckCommands() throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.CHECK);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, null, null, null);
  }

  /**
   * This function lists all the event commands registered within Icinga 2 Monitor.
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listEventCommands() throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.EVENT);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, null, null, null);
  }

  /**
   * This function lists all the notification commands registered within Icinga 2 Monitor.
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNotificationCommands() throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.NOTIFY);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, null, null, null);
  }


  /**
   * This function lists all the services associated to nodes registered within Icinga 2 Monitor.
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodesServices() throws IOException {
    log.info("Prepare a GET request to list all services on all nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.SERVICE);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, null, null, null);
  }

  /**
   * This function lists all the services of a specific node.
   *
   * @param nodeName        to fetch services of
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodeServices(String nodeName) throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.SERVICE);
    return sendRequest(url, new String[]{NAME_QUERY_STRING}, new String[]{SERVICE_BY_HOST_QUERY_STRING
        + URLEncoder.encode("\"" + nodeName + "\"", "UTF-8")}, null, null);
  }


  /**
   * This function lists all the settings of a specific node.
   *
   * @param nodeName        to fetch service configuration of
   * @param serviceName     to fetch configuration of
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodeServiceConfiguration(String nodeName, String serviceName)
      throws IOException {
    return listNodeServiceConfiguration(nodeName + "!" + serviceName);
  }

  /**
   * This function lists a service configuration by its id <hostname>!<serviceName>.
   *
   * @param id              to fetch service configuration of
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response listNodeServiceConfiguration(String id)
      throws IOException {
    log.info("Prepare a GET request to list all the nodes.");
    // Create the URL
    String url = createUrl(server.getUrl(), Icinga2SupportedRequestType.SERVICE)
        + "/" + id;
    return sendRequest(url, null, null, null, null);
  }
}
