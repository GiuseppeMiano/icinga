package it.unimore.monitor.client;

import org.apache.http.auth.UsernamePasswordCredentials;

/*
=================================================================================================
Created on 27/10/2017
*
Icinga2.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2 {
  private final String url;
  private final UsernamePasswordCredentials credentials;
  
  protected Icinga2(String url, UsernamePasswordCredentials credentials) {
    this.url = url;
    this.credentials = credentials;
  }

  /**
   * Return the Icinga 2 server access url
   * @return  url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Get icinga2 login credentials.
   * @return  credentials
   */
  public UsernamePasswordCredentials getCredentials() {
    return credentials;
  }
}
