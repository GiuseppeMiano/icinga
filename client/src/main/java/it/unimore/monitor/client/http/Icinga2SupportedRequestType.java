package it.unimore.monitor.client.http;

/*
=================================================================================================
Created on 29/10/2017
*
Icinga2SupportedRequestType.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public enum Icinga2SupportedRequestType {
  HOST("objects/hosts"),
  SERVICE("objects/services"),
  CHECK("objects/checkcommands"),
  NOTIFY("objects/notificationcommands"),
  EVENT("objects/eventcommands");

  private String type;

  Icinga2SupportedRequestType(String type) {
    this.type = type;
  }

  public String getType() {
    return this.type;
  }

  public static Icinga2SupportedRequestType fromString(String type) {
    for(Icinga2SupportedRequestType t: Icinga2SupportedRequestType.values()) {
      if (t.getType().equalsIgnoreCase("objects/" + type)) {
        return t;
      }
    }
    return null;
  }
}
