package it.unimore.monitor.client.http;

import it.unimore.monitor.client.Icinga2;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 01/11/2017
*
Icinga2DeleteRequest.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2DeleteRequest extends Icinga2RequestBase {
  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);

  Icinga2DeleteRequest(Icinga2 server, CloseableHttpClient httpClient) {
    super(server, httpClient);
  }

  Icinga2Response sendRequest(String url, boolean cascade) throws IOException {
    if (cascade) {
      url += "?cascade=1";
    }
    HttpDelete req = new HttpDelete(url);
    log.info("The url to use is: " + url);
    // Set Required header
    req.addHeader("Accept", "application/json");
    log.info("Sending the request...");
    CloseableHttpResponse response = httpClient.execute(req);
    log.info("...request completed.");
    log.info("The received object is: " + response.toString());
    return new Icinga2Response(response);
  }

  /**
   * This method can be used to delete a node, previously registered within Icinga.
   *
   * @param nodeName      of the host that will be removed
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response removeNode(String nodeName) throws IOException {
    log.info("Prepare a DELETE request to remove the node: " + nodeName + " from the monitoring system.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.HOST,
        nodeName);
    return sendRequest(url, true);
  }

  public Icinga2Response removeNode(String nodeName, boolean cascade) throws IOException {
    log.info("Prepare a DELETE request to remove the node: " + nodeName + " from the monitoring system.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.HOST,
        nodeName);
    return sendRequest(url, cascade);
  }

  /**
   * This method can be used to delete a service, previously registered to a node within Icinga.
   *
   * @param nodeName      of the host from which the service will be removed
   * @param serviceName   of the service to remove
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response removeServiceFromNode(String nodeName, String serviceName)
      throws IOException {
    log.info("Prepare a DELETE request to remove the service: " + serviceName + " from the node: " + nodeName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.SERVICE,
        nodeName + "!" + serviceName);
    return sendRequest(url, true);
  }

  public Icinga2Response removeServiceFromNode(String nodeName, String serviceName, boolean cascade)
      throws IOException {
    log.info("Prepare a DELETE request to remove the service: " + serviceName + " from the node: " + nodeName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.SERVICE,
        nodeName + "!" + serviceName);
    return sendRequest(url, cascade);
  }

  /**
   * This function deletes a command for a given type.
   *
   * @param commandName     to fetch configuration of
   * @param type            Icinga2SupportedRequestType string
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response removeCommand(String commandName, Icinga2SupportedRequestType type)
      throws IOException {
    log.info("Prepare a DELETE request to delete the command: " + commandName + ".");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, true);
  }

  public Icinga2Response removeCommand(String commandName, Icinga2SupportedRequestType type, boolean cascade)
      throws IOException {
    log.info("Prepare a DELETE request to delete the command: " + commandName + ".");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, cascade);
  }

  /**
   * This method can be used to delete a registered check command within Icinga.
   *
   * @param commandName   to unregister from Icinga
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  Icinga2Response removeCheckCommand (String commandName) throws IOException {
    log.info("Prepare a DELETE request to remove the check command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.CHECK,
        commandName);
    return sendRequest(url, true);
  }

  Icinga2Response removeCheckCommand (String commandName, boolean cascade) throws IOException {
    log.info("Prepare a DELETE request to remove the check command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.CHECK,
        commandName);
    return sendRequest(url, cascade);
  }

  /**
   * This method can be used to delete a registered notification command within Icinga.
   *
   * @param commandName   to unregister from Icinga
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  Icinga2Response removeNotificationCommand (String commandName) throws IOException {
    log.info("Prepare a DELETE request to remove the notification command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.NOTIFY,
        commandName);
    return sendRequest(url, true);
  }

  Icinga2Response removeNotificationCommand (String commandName, boolean cascade) throws IOException {
    log.info("Prepare a DELETE request to remove the notification command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.NOTIFY,
        commandName);
    return sendRequest(url, cascade);
  }

  /**
   * This method can be used to delete a registered event command within Icinga.
   *
   * @param commandName   to unregister from Icinga
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  Icinga2Response removeEventCommand (String commandName) throws IOException {
    log.info("Prepare a DELETE request to remove the event command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.EVENT,
        commandName);
    return sendRequest(url, true);
  }

  Icinga2Response removeEventCommand (String commandName, boolean cascade) throws IOException {
    log.info("Prepare a DELETE request to remove the event command: " + commandName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.EVENT,
        commandName);
    return sendRequest(url, cascade);
  }
}
