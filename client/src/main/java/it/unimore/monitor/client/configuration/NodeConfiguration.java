package it.unimore.monitor.client.configuration;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
=================================================================================================
Created on 10/12/2017
*
NodeConfiguration.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class NodeConfiguration implements IConfiguration{
  @SerializedName("display_name")
  private String displayName;
  private String address;
  private String address6;
  @Expose(serialize = false, deserialize = false)
  private JsonArray groups;
  private JsonObject vars = new JsonObject();
  @SerializedName("check_command")
  private String checkCommand = "hostalive";
  @SerializedName("max_check_attempts")
  private Integer maxCheckAttempts;
  @SerializedName("check_period")
  private String checkPeriod;
  @SerializedName("check_timeout")
  private Integer checkTimeout = 60;
  @SerializedName("check_interval")
  private Integer checkInterval;
  @SerializedName("retry_interval")
  private Integer retryInterval;
  @SerializedName("enable_notifications")
  private Boolean enableNotifications;
  @SerializedName("enable_active_checks")
  private Boolean enableActiveChecks;
  @SerializedName("enable_passive_checks")
  private Boolean enablePassiveChecks;
  @SerializedName("enable_event_handler")
  private Boolean enableEventHandler;
  @SerializedName("enable_flapping")
  private Boolean enableFlapping;
  @SerializedName("enable_perfdata")
  private Boolean enablePerfdata;
  @SerializedName("event_command")
  private String eventCommand;
  @SerializedName("flapping_threshold")
  private Double flappingThreshold;
  @SerializedName("volatile")
  private Boolean vol;
  @Expose(serialize = false, deserialize = false)
  private String zone;
  @SerializedName("command_endpoint")
  private String commandEndpoint;
  private String notes;
  @SerializedName("notes_url")
  private String notesUrl;
  @SerializedName("action_url")
  private String actionUrl;
  @SerializedName("icon_image")
  private String iconImage;
  @SerializedName("icon_image_alt")
  private String iconImageAlt;

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getAddress6() {
    return address6;
  }

  public void setAddress6(String address6) {
    this.address6 = address6;
  }

  public JsonArray getGroups() {
    return groups;
  }

  public void setGroups(JsonArray groups) {
    this.groups = groups;
  }

  public JsonObject getVars() {
    return vars;
  }

  public void setVars(JsonObject vars) {
    this.vars = vars;
  }

  public String getCheckCommand() {
    return checkCommand;
  }

  public void setCheckCommand(String checkCommand) {
    this.checkCommand = checkCommand;
  }

  public Integer getMaxCheckAttempts() {
    return maxCheckAttempts;
  }

  public void setMaxCheckAttempts(Integer maxCheckAttempts) {
    this.maxCheckAttempts = maxCheckAttempts;
  }

  public String getCheckPeriod() {
    return checkPeriod;
  }

  public void setCheckPeriod(String checkPeriod) {
    this.checkPeriod = checkPeriod;
  }

  public Integer getCheckTimeout() {
    return checkTimeout;
  }

  public void setCheckTimeout(Integer checkTimeout) {
    this.checkTimeout = checkTimeout;
  }

  public Integer getCheckInterval() {
    return checkInterval;
  }

  public void setCheckInterval(Integer checkInterval) {
    this.checkInterval = checkInterval;
  }

  public Integer getRetryInterval() {
    return retryInterval;
  }

  public void setRetryInterval(Integer retryInterval) {
    this.retryInterval = retryInterval;
  }

  public Boolean getEnableNotifications() {
    return enableNotifications;
  }

  public void setEnableNotifications(Boolean enableNotifications) {
    this.enableNotifications = enableNotifications;
  }

  public Boolean getEnableActiveChecks() {
    return enableActiveChecks;
  }

  public void setEnableActiveChecks(Boolean enableActiveChecks) {
    this.enableActiveChecks = enableActiveChecks;
  }

  public Boolean getEnablePassiveChecks() {
    return enablePassiveChecks;
  }

  public void setEnablePassiveChecks(Boolean enablePassiveChecks) {
    this.enablePassiveChecks = enablePassiveChecks;
  }

  public Boolean getEnableEventHandler() {
    return enableEventHandler;
  }

  public void setEnableEventHandler(Boolean enableEventHandler) {
    this.enableEventHandler = enableEventHandler;
  }

  public Boolean getEnableFlapping() {
    return enableFlapping;
  }

  public void setEnableFlapping(Boolean enableFlapping) {
    this.enableFlapping = enableFlapping;
  }

  public Boolean getEnablePerfdata() {
    return enablePerfdata;
  }

  public void setEnablePerfdata(Boolean enablePerfdata) {
    this.enablePerfdata = enablePerfdata;
  }

  public String getEventCommand() {
    return eventCommand;
  }

  public void setEventCommand(String eventCommand) {
    this.eventCommand = eventCommand;
  }

  public Double getFlappingThreshold() {
    return flappingThreshold;
  }

  public void setFlappingThreshold(Double flappingThreshold) {
    this.flappingThreshold = flappingThreshold;
  }

  public Boolean getVol() {
    return vol;
  }

  public void setVol(Boolean vol) {
    this.vol = vol;
  }

  public String getZone() {
    return zone;
  }

  public void setZone(String zone) {
    this.zone = zone;
  }

  public String getCommandEndpoint() {
    return commandEndpoint;
  }

  public void setCommandEndpoint(String commandEndpoint) {
    this.commandEndpoint = commandEndpoint;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getNotesUrl() {
    return notesUrl;
  }

  public void setNotesUrl(String notesUrl) {
    this.notesUrl = notesUrl;
  }

  public String getActionUrl() {
    return actionUrl;
  }

  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }

  public String getIconImage() {
    return iconImage;
  }

  public void setIconImage(String iconImage) {
    this.iconImage = iconImage;
  }

  public String getIconImageAlt() {
    return iconImageAlt;
  }

  public void setIconImageAlt(String iconImageAlt) {
    this.iconImageAlt = iconImageAlt;
  }
}
