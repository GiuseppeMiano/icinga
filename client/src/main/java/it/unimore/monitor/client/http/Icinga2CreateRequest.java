package it.unimore.monitor.client.http;

import com.google.gson.JsonArray;
import it.unimore.monitor.client.Icinga2;
import it.unimore.monitor.client.Icinga2ClientBuilder;
import it.unimore.monitor.client.configuration.CommandConfiguration;
import it.unimore.monitor.client.configuration.IConfiguration;
import it.unimore.monitor.client.configuration.NodeConfiguration;
import it.unimore.monitor.client.configuration.ServiceConfiguration;
import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 28/10/2017
*
Icinga2CreateRequest.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2CreateRequest extends Icinga2RequestBase {
  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);

  Icinga2CreateRequest(Icinga2 server, CloseableHttpClient httpClient) {
    super(server, httpClient);
  }

  Icinga2Response sendRequest(String url, IConfiguration attrs, JsonArray templates)
      throws IOException {
    HttpPut req = new HttpPut(url);
    log.info("The url to use is: " + url);
    // Set Required header
    req.addHeader("Accept", "application/json");
    // Set Request attrs
    req.setEntity(createBasePayload(attrs, templates));
    log.info("Sending the request...");
    CloseableHttpResponse response = httpClient.execute(req);
    log.info("...request completed.");
    log.info("The received object is: " + response.toString());
    return new Icinga2Response(response);
  }

  /**
   * This method can be used to register a node within Icinga.
   *
   * @param nodeName      to assign to the host to register
   * @param attrs       configuration of the new host
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response addNode (String nodeName, NodeConfiguration attrs)
      throws IOException {
    // Default templates
    JsonArray templates = new JsonArray();
    return addNode(nodeName, attrs, templates);
  }

  public Icinga2Response addNode (String nodeName, NodeConfiguration attrs, JsonArray templates)
      throws IOException {
    log.info("Prepare a PUT request to add the node: " + nodeName + " into the monitoring system.");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.HOST, nodeName);
    return sendRequest(url, attrs, templates);
  }

  /**
   * This method can be used to assign a service to a node registered within Icinga.
   *
   * @param nodeName      host to which assign a service
   * @param serviceName   to assign to the host
   * @param attrs       configuration of the new service
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response addServiceToNode (String nodeName, String serviceName, ServiceConfiguration attrs)
      throws IOException {
    // Default templates
    JsonArray templates = new JsonArray();
    return addServiceToNode(nodeName, serviceName, attrs, templates);
  }

  public Icinga2Response addServiceToNode (String nodeName, String serviceName,
      ServiceConfiguration attrs, JsonArray templates)
      throws IOException {
    log.info("Prepare a PUT request to register the service: " + serviceName +
        " into the node: " + nodeName);
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.SERVICE,
        nodeName + "!" + serviceName);
    return sendRequest(url, attrs, templates);
  }

  /**
   * This function add a command for a given type.
   *
   * @param commandName     to fetch configuration of
   * @param attrs       configuration of the new command
   * @param type            Icinga2SupportedRequestType string
   *
   * @return  response      HttpResponse object, parsable to get response body and other parameters
   * @throws  IOException   if the request fails
   */
  public Icinga2Response addCommand(String commandName, CommandConfiguration attrs, Icinga2SupportedRequestType type)
      throws IOException {
    log.info("Prepare a PUT request to add the command: " + commandName + ".");
    // Default templates
    JsonArray templates = new JsonArray();
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, attrs, templates);
  }

  public Icinga2Response addCommand(String commandName, CommandConfiguration attrs,
      Icinga2SupportedRequestType type, JsonArray templates) throws IOException {
    log.info("Prepare a DELETE request to delete the command: " + commandName + ".");
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), type, commandName);
    return sendRequest(url, attrs, templates);
  }

  /**
   * This method can be used to create a new check command (see https://www.icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#check-commands)
   *
   * @param commandName   to assign to the command that will be registered
   * @param attrs       configuration of the new command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response addCheckCommand (String commandName, CommandConfiguration attrs) throws IOException {
    // Default templates
    JsonArray templates = new JsonArray();
    return addCheckCommand(commandName, attrs, templates);
  }

  public Icinga2Response addCheckCommand (String commandName, CommandConfiguration attrs,
      JsonArray templates) throws IOException {
    log.info("Prepare a PUT request to register the new service: " + commandName);
    log.info("The service will be configured as: " + gsonBase.toJson(attrs));
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.CHECK,
        commandName);
    return sendRequest(url, attrs, templates);
  }

  /**
   * This method can be used to create a new notification command (see https://www.icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#notification-commands)
   *
   * @param commandName   to assign to the command that will be registered
   * @param attrs       configuration of the new command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response addNotificationCommand (String commandName, CommandConfiguration attrs) throws IOException {
    // Default templates
    JsonArray templates = new JsonArray();
    return addNotificationCommand(commandName, attrs, templates);
  }
  public Icinga2Response addNotificationCommand (String commandName, CommandConfiguration attrs,
      JsonArray templates) throws IOException {
    log.info("Prepare a PUT request to register the new service: " + commandName);
    log.info("The service will be configured as: " + gsonBase.toJson(attrs));
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.NOTIFY,
        commandName);
    return sendRequest(url, attrs, templates);
  }


  /**
   * This method can be used to create a new event command (see https://www.icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#event-commands)
   *
   * @param commandName   to assign to the command that will be registered
   * @param attrs       configuration of the new command
   * @return response     http response containing data returned from the server
   * @throws IOException
   */
  public Icinga2Response addEventCommand (String commandName, CommandConfiguration attrs) throws IOException {
    // Default templates
    JsonArray templates = new JsonArray();
    return addEventCommand(commandName, attrs, templates);
  }

  public Icinga2Response addEventCommand (String commandName, CommandConfiguration attrs,
      JsonArray templates) throws IOException {
    log.info("Prepare a PUT request to register the new service: " + commandName);
    log.info("The service will be configured as: " + gsonBase.toJson(attrs));
    // Create the URL
    String url = createUrlWithResource(server.getUrl(), Icinga2SupportedRequestType.EVENT,
        commandName);
    return sendRequest(url, attrs, templates);
  }
}
