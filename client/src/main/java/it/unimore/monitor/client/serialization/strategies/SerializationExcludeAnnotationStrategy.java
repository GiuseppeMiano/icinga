package it.unimore.monitor.client.serialization.strategies;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.annotations.Expose;

/*
=================================================================================================
Created on 10/12/2017
*
SerializationExcludeAnnotationStrategy.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class SerializationExcludeAnnotationStrategy implements ExclusionStrategy{

  @Override
  public boolean shouldSkipField(FieldAttributes fieldAttributes) {
    Expose exclude = fieldAttributes.getAnnotation(Expose.class);
    boolean skip = false;
    if (exclude != null && !exclude.serialize()) {
      skip = true;
    }
    return skip;
  }

  @Override
  public boolean shouldSkipClass(Class<?> aClass) {
    return false;
  }
}
