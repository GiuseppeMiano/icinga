package it.unimore.monitor.client.http;

import java.io.IOException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

/*
=================================================================================================
Created on 04/11/2017
*
Icinga2Response.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2Response {
  private int responseCode;
  private String responseReason;
  private String responsePayload;

  public Icinga2Response(CloseableHttpResponse response) throws IOException {
    this.responseCode = response.getStatusLine().getStatusCode();
    this.responseReason = response.getStatusLine().getReasonPhrase();
    this.responsePayload = EntityUtils.toString(response.getEntity(), "UTF-8");
    response.close();
  }

  public String getResponsePayload() throws IOException {
    return responsePayload;
  }

  /**
   * This method parses an Http Response and returns the response code
   * @return  code    Response code
   */
  public int getResponseCode() {
    return responseCode;
  }

  /**
   * This method parses an Http Response and returns the response reason
   * @return  reason  Response reason
   */
  public String getResponseReason() {
    return responseReason;
  }
}
