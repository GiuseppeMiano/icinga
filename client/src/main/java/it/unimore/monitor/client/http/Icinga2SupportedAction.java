package it.unimore.monitor.client.http;

/*
=================================================================================================
Created on 28/10/2017
*
Icinga2SupportedAction.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public enum Icinga2SupportedAction {
  CREATE,
  EDIT,
  DELETE,
  LIST
}
