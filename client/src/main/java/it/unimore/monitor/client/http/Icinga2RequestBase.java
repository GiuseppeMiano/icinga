package it.unimore.monitor.client.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.unimore.monitor.client.Icinga2;
import it.unimore.monitor.client.configuration.IConfiguration;
import it.unimore.monitor.client.serialization.strategies.DeserializationExcludeAnnotationStrategy;
import it.unimore.monitor.client.serialization.strategies.SerializationExcludeAnnotationStrategy;
import java.io.UnsupportedEncodingException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

/*
=================================================================================================
Created on 29/10/2017
*
Icinga2RequestBase.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
abstract class Icinga2RequestBase {
  final static Gson gsonBase = new Gson();
  final static Gson gsonWithExclusion = new GsonBuilder()
      .addDeserializationExclusionStrategy(new DeserializationExcludeAnnotationStrategy())
      .addSerializationExclusionStrategy(new SerializationExcludeAnnotationStrategy()).create();
  final static JsonParser parser = new JsonParser();
  final Icinga2 server;
  final CloseableHttpClient httpClient;

  Icinga2RequestBase(Icinga2 server, CloseableHttpClient httpClient) {
    this.server = server;
    this.httpClient = httpClient;
  }

  /**
   * Given a base url this method must build a valid url string through which a specific request
   * can be sent to Icinga2 REST API
   *
   * @param   baseUrl     Base url to access Icinga2 https://hostname:port
   * @param   type        REST method path
   * @return  url         to which a request can be sent
   */
  String createUrl(String baseUrl, Icinga2SupportedRequestType type) {
    return baseUrl + "/" + type.getType();
  }

  /**
   * Given a base url and a resource path this method must build a valid url string through which
   * a specific request can be sent to Icinga2 REST API
   *
   * @param   baseUrl         Base url to access Icinga2 https://hostname:port
   * @param   type            REST method path
   * @param   resourceName    Resource name
   * @return  url             to which a request can be sent
   */
  String createUrlWithResource(String baseUrl, Icinga2SupportedRequestType type, String resourceName) {
    return createUrl(baseUrl, type) + "/" + resourceName;
  }

  /**
   * Parse a JSON object and return a string representing the JSON object. The base parser is used
   *
   * @param attrs          to convert to string
   * @return attrsObj
   */
  StringEntity createBasePayload(IConfiguration attrs) throws UnsupportedEncodingException {
    return createBasePayload(attrs, gsonBase);
  }

  /**
   * Parse a JSON object and return a string representing the JSON object. The exclusion parser is used
   *
   * @param attrs          to convert to string
   * @return attrsObj
   */
  StringEntity createExclusionPayload(IConfiguration attrs) throws UnsupportedEncodingException {
    return createBasePayload(attrs, gsonWithExclusion);
  }

  private StringEntity createBasePayload(IConfiguration attrs, Gson gson)
      throws UnsupportedEncodingException {
    JsonObject requestPayload = new JsonObject();
    requestPayload.add("attrs", parser.parse(gson.toJson(attrs)).getAsJsonObject());
    String json = gson.toJson(requestPayload);
    return new StringEntity(json);
  }

  StringEntity createBasePayload(IConfiguration attrs, JsonArray templates) throws UnsupportedEncodingException {
    return createBasePayload(attrs, templates, gsonBase);
  }

  StringEntity createExclusionPayload(IConfiguration attrs, JsonArray templates) throws UnsupportedEncodingException {
    return createBasePayload(attrs, templates, gsonWithExclusion);
  }

  private StringEntity createBasePayload(IConfiguration attrs, JsonArray templates, Gson gson)
      throws UnsupportedEncodingException {
    JsonObject requestPayload = new JsonObject();
    requestPayload.add("attrs", parser.parse(gson.toJson(attrs)).getAsJsonObject());
    requestPayload.add("templates", templates);
    String json = gson.toJson(requestPayload);
    return new StringEntity(json);
  }
}
