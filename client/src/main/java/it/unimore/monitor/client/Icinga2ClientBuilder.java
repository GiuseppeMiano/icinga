package it.unimore.monitor.client;

import java.io.IOException;
import java.net.MalformedURLException;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.log4j.Logger;

/*
=================================================================================================
Created on 27/10/2017
*
Icinga2ClientBuilder.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2ClientBuilder {

  private final static Logger log = Logger.getLogger(Icinga2ClientBuilder.class);
  private final static String PROTOCOL = "https";
  private final static String[] SCHEMES = {"https"};
  private final static String VERSION = "v1";
  private final static UrlValidator urlValidator =
      new UrlValidator(SCHEMES, UrlValidator.ALLOW_LOCAL_URLS);

  private String hostname = "localhost";
  private int port = 5665;
  private UsernamePasswordCredentials credentials =
      new UsernamePasswordCredentials("root", "icinga");

  // It cannot be initialized.
  private Icinga2ClientBuilder() {
    // no-op
  }

  /**
   * @return Create new instance of builder with all defaults set.
   */
  public static Icinga2ClientBuilder create() {
    return new Icinga2ClientBuilder();
  }

  /**
   * @return Default client
   */
  public static Icinga2 defaultClient() throws IOException {
    log.info("The default client is built.");
    return create().build();
  }

  /**
   * Build the client
   *
   * @return icinga2
   * @throws MalformedURLException If the URL is not valid.
   */
  public Icinga2 build() throws MalformedURLException {
    log.info("Start building the client...");
    String url = getUrl();
    validate(url);
    return new Icinga2(url, credentials);
  }

  /**
   * This method validates the given url and checks connection.
   *
   * @param url to contact Icinga REST server
   * @throws MalformedURLException If the URL is not valid.
   */
  private void validate(String url) throws MalformedURLException {
    log.info("Validate the server url...");
    if (urlValidator.isValid(url)) {
      log.info("The URL: " + url + " is valid.");
    } else {
      log.error("The URL: " + url + " is not well formed.");
      throw new MalformedURLException();
    }
  }

  /**
   * Set up the hostname in which Icinga is running
   *
   * @param hostname IP or hostname in which Icinga2 is installed
   * @return builder
   */
  public Icinga2ClientBuilder withHostname(String hostname) {
    this.hostname = hostname;
    return this;
  }

  /**
   * Set up the port through which Icinga can be reached
   *
   * @param port Port through which Icinga can be invoked
   * @return builder
   */
  public Icinga2ClientBuilder withPort(int port) {
    this.port = port;
    return this;
  }

  /**
   * Set up credentials to use to contact Icinga2 REST service
   *
   * @param username Registered to Icinga2 Server
   * @param password Required to authenticate to Icinga2 Server
   * @return builder
   */
  public Icinga2ClientBuilder withCredentials(String username, String password) {
    this.credentials = new UsernamePasswordCredentials(username, password);
    return this;
  }

  /**
   * This method is a constructor to build a URL given the hostname, protocol and port.
   *
   * @return url
   */
  private String getUrl() {
    return PROTOCOL + "://" + hostname + ":" + port + "/" + VERSION;
  }
}
