package it.unimore.monitor.client.http;

import it.unimore.monitor.client.Icinga2;
import org.apache.http.auth.AuthScope;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/*
=================================================================================================
Created on 28/10/2017
*
Icinga2RequestFactory.java
*
Comments are welcome.
*
Author: peppe

=================================================================================================
*/
public class Icinga2RequestFactory {

  private final Icinga2 server;
  private final CloseableHttpClient httpClient;


  /**
   * Base constructor to initialize the factory
   * @param server    Icinga2 client, correctly built
   */
  public Icinga2RequestFactory (Icinga2 server) {
    // Set Basic Authentication
    CredentialsProvider provider = new BasicCredentialsProvider();
    provider.setCredentials(AuthScope.ANY, server.getCredentials());
    this.server = server;
    // Create http client
    httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
  }

  /**
   * @param action    One of the supported actions (see Icinga2SupportedAction class)
   * @return request  A Icinga 2 request object, through which REST methods can be called
   */
  public Icinga2RequestBase getRequest(Icinga2SupportedAction action) throws Exception {
    switch (action) {
      case CREATE: return new Icinga2CreateRequest(server, httpClient);
      case LIST: return new Icinga2ListRequest(server, httpClient);
      case DELETE: return new Icinga2DeleteRequest(server, httpClient);
      case EDIT: return new Icinga2EditRequest(server, httpClient);
      default: throw new Exception("Unsupported Request Type.");
    }
  }
}
